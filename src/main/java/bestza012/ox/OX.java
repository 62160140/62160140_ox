/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bestza012.ox;

/**
 *
 * @author WIN10
 */
import java.util.Scanner;

public class OX {

    public static void showarr(String[][] arr) {
        System.out.println("  1 2 3");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(i + 1 + " ");
            for (int j = 0; j < arr.length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public static void main(String[] args) {
        String arr[][] = new String[3][3];
        Scanner kb = new Scanner(System.in);
        System.out.println("------Welcome to ||OX|| Game------");
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                arr[i][j] = "-";
            }
        }

        showarr(arr);
        
        int counto=0;
        int countx=0;
        
        String win = "draw";
        while (win.equals("draw")) {
            
            System.out.println("X turn");
            System.out.println("Please input Row Col: ");
            int rowx = kb.nextInt();
            int colx = kb.nextInt();
            arr[rowx - 1][colx - 1] = "X";
            showarr(arr);
            if (arr[0][0].equals("X") && arr[0][1].equals("X") && arr[0][2].equals("X")) {
                win = "win";
                System.out.println("X win");
                break;
            }else if (arr[1][0].equals("X") && arr[1][1].equals("X") && arr[1][2].equals("X")) {
                win = "win";
                System.out.println("X win");
                break;
            }else if (arr[2][0].equals("X") && arr[2][1].equals("X") && arr[2][2].equals("X")) {
                win = "win";
                System.out.println("X win");
                break;
            }else if (arr[0][0].equals("X") && arr[1][0].equals("X") && arr[2][0].equals("X")) {
                win = "win";
                System.out.println("X win");
                break;
            }else if (arr[0][1].equals("X") && arr[1][1].equals("X") && arr[2][1].equals("X")) {
                win = "win";
                System.out.println("X win");
                break;
            }else if (arr[0][2].equals("X") && arr[1][2].equals("X") && arr[2][2].equals("X")) {
                win = "win";
                System.out.println("X win");
                break;
            }else if (arr[0][0].equals("X") && arr[1][1].equals("X") && arr[2][2].equals("X")) {
                win = "win";
                System.out.println("X win");
                break;
            }else if (arr[0][2].equals("X") && arr[1][1].equals("X") && arr[2][0].equals("X")) {
                win = "win";
                System.out.println("X win");
                break;
            }
            countx++;
            
             if(countx==5&&counto==4){
                System.out.println("This game is draw.");
                break;
            }

            System.out.println("O turn");
            System.out.println("Please input Row Col: ");
            int rowo = kb.nextInt();
            int colo = kb.nextInt();
            arr[rowo - 1][colo - 1] = "O";
            showarr(arr);
            if (arr[0][0].equals("O") && arr[0][1].equals("O") && arr[0][2].equals("O")) {
                win = "win";
                System.out.println("O win");
                break;
            }else if (arr[1][0].equals("O") && arr[1][1].equals("O") && arr[1][2].equals("O")) {
                win = "win";
                System.out.println("O win");
                break;
            }else if (arr[2][0].equals("O") && arr[2][1].equals("O") && arr[2][2].equals("O")) {
                win = "win";
                System.out.println("O win");
                break;
            }else if (arr[0][0].equals("O") && arr[1][0].equals("O") && arr[2][0].equals("O")) {
                win = "win";
                System.out.println("O win");
                break;
            }else if (arr[0][1].equals("O") && arr[1][1].equals("O") && arr[2][1].equals("O")) {
                win = "win";
                System.out.println("O win");
                break;
            }else if (arr[0][2].equals("O") && arr[1][2].equals("O") && arr[2][2].equals("O")) {
                win = "win";
                
                System.out.println("O win");
                break;
            }else if (arr[0][0].equals("O") && arr[1][1].equals("O") && arr[2][2].equals("O")) {
                win = "win";
                System.out.println("O win");
                break;
            }else if (arr[0][2].equals("O") && arr[1][1].equals("O") && arr[2][0].equals("O")) {
                win = "win";
                System.out.println("O win");
                break;
            }
            counto++;
            
        }
        
        System.out.println("Bye Bye . .  .");
    }
}
